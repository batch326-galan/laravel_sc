@extends('layouts.app')

@section('tabName')
    {{$post->title}}
@endsection

@section('content')

    <div class="card col-6 mx-auto">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class='card-subtitle text-muted mb-3'>Created at: {{$post->created_at}}</p>
            <h4>Content:</h4>
            <p class="card-text">{{$post->body}}</p>

            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains('user_id', Auth::id()))
                        <button class="btn btn-danger">Unlike</button>
                    @else
                        <button class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif

            <br/>
            <a href="/posts/{{$post->id}}/comments" class="btn btn-primary mt-2">Comment</a>

            <br/>
            <a href="/posts" class="btn btn-info mt-2">View all posts</a>
        </div>
    </div>

    <div class="card col-6 mx-auto mt-4">
        <div class="card-body">
            <h4>Comments:</h4>
            @if(count($post->comments) > 0)
                @foreach($post->comments as $comment)
                    <div class="comment">
                        <p>{{$comment->body}}</p>
                        <p class="text-muted">By: {{$comment->user->name}}</p>
                    </div>
                    <hr>
                @endforeach
            @else
                <p>No comments yet.</p>
            @endif
        </div>
    </div>

@endsection