@extends('layouts.app')

@section('tabName')
	{{$post->title}}
@endsection

@section('content')
	
	<div class = "card col-6 mx-auto">
		<div class = 'card-body'>
			<h2 class = 'card-title'>{{$post->title}}</h2>
			<p class = "card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class = 'card-subtitle text-muted mb-3'>Created at: {{$post->created_at}}</p>
			<h4>Content:</h4>
			<p class = "card-text">{{$post->body}}</p>

			<form action="{{ route('comments.store', $post) }}" method="POST">
			    @csrf
			    <div class="form-group">
			        <label for="content">Add a comment:</label>
			        <textarea class="form-control" name="body" id="body" rows="3" required></textarea>
			    </div>
			    <button type="submit" class="btn btn-primary mt-2">Add Comment</button>
			</form>

		</div>		
	</div>

@endsection	