<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostController;
use App\Models\Post;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts = Post::all();
    return view('welcome')->with('posts', $posts);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//This route is for logging out.
Route::get('/logout', [PostController::class, 'logout']);


//This route is for creation of a new post:
Route::get('/posts/create', [PostController::class, 'createPost']);

//This route is for saving the post on our database:
Route::post('/posts', [PostController::class, 'savePost']);


//This route is for the list of post on our database:
Route::get('/posts', [PostController::class, 'showPosts']);


//Define a route that will return a view containing only the authenticated user's post
Route::get('/myPosts', [PostController::class, 'myPosts']);

//define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

//define a route wherein a view to edit a post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

//define a route that will overwrite an existing post with matching URL parameter ID via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

//define a route that will delete a post of the matching URL parameter ID
Route::delete('/posts/{id}', [PostController::class, 'archive']);

//Define a web rout that will call the function for liking and unliking a specific post:
Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::get('/posts/{id}/comments', [PostController::class, 'comment']);
Route::post('/posts/{id}/comments', [PostController::class, 'store'])->name('comments.store');


