<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{

    //Controller is for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();
        return redirect('/login');
    }

    //action to return a view containing a form for a blog post creation
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
        //to check whether there is an authenticated user or none
        if(Auth::user()){
            //instantiate a new Post Oject from the Post method and then save it in $post variable:
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title=$request->input('title');
            $post->body=$request->input('content');

            //this will get the id of the authenticated user and set it as the foreign jet user_id of the new post.
            $post->user_id=(Auth::user()->id);

            //save the post object in our Post Table;
            $post->save();

            return redirect('/posts');
        }else{
            return redirect('/login');
        }
    }

    //controller will return all the the blog posts
    public function showPosts(){
        //the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::all();

        return view('posts.showPosts')->with('posts', $posts);
    }

    //action for showing only the posts authored by authenticated user
    public function myPosts(){
        if(Auth::user()){
            
            $posts = Auth::user()->posts;

            return view('posts.showPosts')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }

    //action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown
  

    public function show($id){
        $post = Post::with('comments')->find($id);
        return view('posts.show', compact('post'));
    }

    //action that will return a view showing an edit form for a specific post using the URL parameter $id to query for the database entry to be edited
        public function edit($id)
        {
            $post = Post::find($id);
            return view('posts.edit')->with('post', $post);
        }

        public function update(Request $request, $id){
                $post = Post::find($id);
                //if authenticated user's ID is the same as the post's user_id
                if(Auth::user()->id == $post->user_id){
                    $post->title = $request->input('title');
                    $post->body = $request->input('body');
                    $post->save();  
                }
                return redirect('/posts');
            }

        public function archive($id){
                $post = Post::find($id);
                //if authenticated user's ID is the same as the post's user_id
                if(Auth::user()->id == $post->user_id){
                    $post->isActive = false;
                    $post->save();  
                }
                return redirect('/posts');
            }


        public function like($id){
            $post = Post::find($id);

            if($post->user_id != Auth::user()->id){

                if($post->likes->contains("user_id", Auth::user()->id)){
                    //delete the like record made by the user before
                    PostLike::where('post_id', $post->id)->where('user_id', Auth::user()->id)->delete();
                }else{
                    //create a new like record to like this post
                    //instantiate a new PostLike object from the Postlike model
                    $postlike = new PostLike;
                    //define the properties of the $postlike
                    $postlike->post_id = $post->id;
                    $postlike->user_id = Auth::user()->id;

                    //save this postlike object in the database
                    $postlike->save();
                }
            }

            return redirect("/posts/$id");
        }

        public function comment($id){
            
            $post = Post::find($id);
            
            return view('posts.comment')->with('post', $post);
        }

        

        public function store(Request $request, $id)
        {
            $post = Post::find($id);

            if ($post->user_id != Auth::user()->id) {
                $postComment = new PostComment;
                $postComment->post_id = $post->id;
                $postComment->body = $request->input('body');
                $postComment->user_id = Auth::user()->id;

                $postComment->save();
            }

            return redirect("/posts/$id");
        }


        public function showComments($postId)
            {
                $post = Post::findOrFail($postId);

                return view('post-comments', compact('post'));
            }


}
